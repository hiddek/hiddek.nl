---
layout: job
title: "Postal Sorter"
company: "PostNL"
location: "Leeuwarden, NL"
logo: /assets/images/jobs/postnl.png
date: 2012-12-01
date_end: 2013-06-01
responsibilities:
  - "Responsible for the preparation and sorting of post and parcels in time for their distribution to the regions serviced by the sorting
center."
---