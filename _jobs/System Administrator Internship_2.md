---
layout: job
title: "System Administrator Internship"
company: "Incassade Deurwaarders en Incasso"
location: "Leeuwarden, NL"
date: 2012-02-01
date_end: 2012-08-01
responsibilities:
  - "Developing and testing of a mobile printing solution."
  - "Providing level 1 IT support to members of staff."
  - "Documenting reported issues and requests in the in-house ticketing system."
  - "Writing clear and concise instructions for members of staff."
  - "Documenting the configuration of the local network."
  - "Monitoring the local network for emerging issues."
---