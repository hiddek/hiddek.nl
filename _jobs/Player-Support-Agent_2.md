---
permalink: "/jobs/:year/:month/Player Support Agent/"
layout: job
title: "Player Support Agent"
company: "Ubisoft CRC Ltd."
location: "Newcastle upon Tyne"
logo: /assets/images/jobs/ubi_black.png
date: 2019-09-01
date_end: 2020-02-01
responsibilities:
  - "Providing specialist advice to colleagues and partners."
  - "​Providing feedback and suggestions on operational processes."
  - "Handling and resolving cases escalated to level 2 and 3 support."
  - "Maintaining the accuracy of the knowledge base."
  - "Taking ownership of reporting and documenting technical and e-commerce
incidents."
  - "Providing excellent Account, Technical, and E-commerce support to players
and customers."
  - "Providing Mental Health First Aid to all members of staff, working to improve general mental well-being in the office."
  - "Translating and localising of public and internal knowledge articles to a high
standard."
---