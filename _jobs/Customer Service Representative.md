---
layout: job
title: "Customer Service Representative"
company: "Cendris"
location: "Leeuwarden, NL"
logo: /assets/images/jobs/cendris.png
date: 2013-03-01
date_end: 2013-05-01
responsibilities:
  - "Responsible for scheduling appointments for the seasonal tire changing for customers of a nationwide car servicing company."
---