---
layout: job
title: "System Administrator Internship"
company: "Pranger Rosier Installaties BV"
location: "Dokkum, NL"
logo: /assets/images/jobs/pranger_rosier.png
date: 2011-02-01
date_end: 2011-08-01
responsibilities:
  - "Setting up the local network and workstations to a new office."
  - "Providing level 1 IT support to members of staff."
  - "Documenting reported issues and requests in the in-house ticketing system."
  - "Writing clear and concise instructions for members of staff."
  - "Monitoring the local network for emerging issues."
---