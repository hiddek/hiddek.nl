---
layout: job
title: "Store Escalations Specialist"
company: "Ubisoft CRC Ltd."
location: "Newcastle upon Tyne"
logo: /assets/images/jobs/ubi_black.png
date: 2019-02-01
date_end: 2020-08-01
responsibilities:
  - "Providing solutions to e-commerce related cases escalated to level 2 and 3
support."
  - "Providing specialist advice to colleagues of all levels."
  - "​Effective reporting and documenting of e-commerce related incidents."
  - "Maintaining the accuracy of the e-commerce knowledge base."
  - "Liaising with e-commerce business teams on behalf of customers."
  - "Providing feedback and suggestions on operational processes."
  - "Providing Mental Health First Aid to all members of staff, working to improve
general mental well-being in the office."
---