---
layout: job
title: "Client Services Advisor"
company: "Central Judicial Collection Agency"
location: "Leeuwarden, NL"
logo: /assets/images/jobs/logo_cjib.svg
date: 2013-06-01
date_end: 2017-03-01
responsibilities:
  - "Answering citizens' queries regarding judicial and criminal penalties."
  - "Rewriting operational procedures for the department to ensure they are clear and concise."
  - "Training new members of the department in both group and one-on- one settings."
  - "Coaching colleagues in the use of operational procedures, soft skills, and handling of difficult calls."
  - "Providing specialist advice to colleagues on complex cases and complaints."
  - "​Rebuilding internal knowledge base from the ground up, improving the efficiency of its use during a live
interaction."
  - "Collaborating with the back-office on behalf of citizens regarding their requests."
---