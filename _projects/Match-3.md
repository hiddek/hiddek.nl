---
layout: project
title:  "Match 3"
intro: "A Match 3 game made using Godot to learn game development."
repository: https://gitlab.com/hiddek/match3
icon: /assets/images/match3/icon.png
---

