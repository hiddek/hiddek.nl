---
layout: post
title:  "Match 3 Matchfinding Algorithm"
author: Hidde Kloosterman
date: 2020-04-19
project: "Match 3"
category: [Projects, "Game Dev", Algorithms]
tags: [Godot, "Flood Fill", "Algorithm", "GDScript"]
---

My initial version of the 'match finding' algorithm for my Match 3 project iterated over all pieces on the grid, and marked any 3 or more consecutive pieces of the same colour as matched. This worked, however, the algorithm is unaware of either the direction of the match, or its length. In preparation for 'special' pieces, I reworked the algorithm to add these features, and increase its efficiency.

A quick browse online turned my attention to the Flood Fill algorithm. An algorithm most commonly used in the 'Paint Bucket' features of your favourite image editing programme. I have implemented a modified version of a flood fill algorithm in the **_find_match()** method, and the **_find_in_direction** helper method. Both are listed below.

The **_find_match()** function takes two parameters. A Vector2 of the root position for the search in the grid, and the colour to match.

#### _find_match()
{: .highlight_heading}

{% highlight GDScript%}
{% raw %}
func _find_match(piece_position: Vector2, match_colour) -> Dictionary:
	
	# Return an empty array if the position in the grid is null
	if _global.grid.gridArray[piece_position.x][piece_position.y] == null:
		return {}
	
	# Return an empty dictionary if the matched variable of the
	# piece in the position has already been set to true
	if _global.grid.gridArray[piece_position.x][piece_position.y].matched:
		return {}
	
	# Initialising the dictionary with arrays for the 
	# horizontal and vertical directions
	var _matched_pieces: Dictionary = {
		"horizontal": [],
		"vertical": [],
	}
	
	# Assign the piece in the root position to the root variable,
	# and add it to the horizontal array
	var piece = _global.grid.gridArray[piece_position.x][piece_position.y]
	_matched_pieces["horizontal"].push_front(piece)

	# Call the helper method to check for matching pieces in the horizontal directions
	_find_in_direction(_matched_pieces["horizontal"], piece_position, Vector2.LEFT, match_colour)
	_find_in_direction(_matched_pieces["horizontal"], piece_position, Vector2.RIGHT, match_colour)

	# Remove all pieces other than the root piece if only 1 other matching piece was found.
	if _matched_pieces["horizontal"].size() == 2:
		_matched_pieces["horizontal"].pop_back()
	
	# Check for every piece in the horizontal array if there are vertical matches
	# Any matched pieces are first added to v_matches[].
	# If more than 2 vertical positions were matched, add them to vertical array in the dictionary
	for matched_piece in _matched_pieces["horizontal"]:

		var v_matches = []

		_find_in_direction(v_matches, matched_piece.grid_position, Vector2.UP, match_colour)
		_find_in_direction(v_matches, matched_piece.grid_position, Vector2.DOWN, match_colour)
		
		if v_matches.size() >= 2:
			_matched_pieces["vertical"] += v_matches

	# Return an empty dictionary if no valid matches were found
	if _matched_pieces["horizontal"].size() + _matched_pieces["vertical"].size() <= 2:
		return {}	

	# If no match was found in the horizontal direction, 
	# check for horizontal matches for every piece in the vertical array
	if _matched_pieces["horizontal"].size() < 3:

		for matched_piece in _matched_pieces["vertical"]:

			var h_matches = []
	
			_find_in_direction(h_matches, matched_piece.grid_position, Vector2.LEFT, match_colour)
			_find_in_direction(h_matches, matched_piece.grid_position, Vector2.RIGHT, match_colour)
			
			if h_matches.size() >= 2:
				_matched_pieces["horizontal"] += h_matches
		
	# If the resulting vertical match is longer than the horizontal match, move the root piece
	# to the front of the vertical array, and remove it from the horizontal array
	if _matched_pieces["horizontal"].size() - 1 < _matched_pieces["vertical"].size():
		_matched_pieces["vertical"].push_front(_matched_pieces["horizontal"][0])
		_matched_pieces["horizontal"].pop_front()

	# Clear the horizontal or vertical array if they do not
	# have enough pieces for a valid match
	for direction in _matched_pieces:
		if _matched_pieces[direction].size() <= 1:
			_matched_pieces[direction].clear()

	return _matched_pieces
{% endraw %}
{% endhighlight %}

The **_find_in_direction()** helper method, takes 4 parameters, an array for the matched pieces, the start position, the desired direction, and the colour to match

#### _find_in_direction()
{: .highlight_heading}

{% highlight GDScript%}
{% raw %}
func _find_in_direction(matched_pieces: Array, start_position: Vector2, direction: Vector2, match_colour: String):
	var position_tracker = Vector2(start_position.x, start_position.y)
	while true:
		
		# Check if we are out of bounds of the grid
		# Break the loop if we are
		match direction:
			Vector2.LEFT:
				if position_tracker.x + direction.x < 0:
					break

			Vector2.RIGHT:
				if position_tracker.x + direction.x > _global.grid.width - 1:
					break

			Vector2.UP:
				if position_tracker.y + direction.y < 0:
					break

			Vector2.DOWN:
				if position_tracker.y + direction.y > _global.grid.height - 1:
					break

		# Assign the piece at the position to be checked
		# and check if it's a match. If not, break the loop
		# If so, move the position tracker forward and add
		# the piece to the matched pieces array
		var piece = _global.grid.gridArray[position_tracker.x + direction.x][position_tracker.y + direction.y]

		if not piece.colour == match_colour:
			break
		else:
			position_tracker += direction
			matched_pieces.append(piece)
{% endraw %}
{% endhighlight %}

This algorithm has the desired results. Each match is now stored individually, and allows for determining their directions.
With this change now implemented, I was able to add logic for creating specials, if a match with a length 4 or 5 was made.

<div>
	<img style="margin-left: auto; margin-right: auto;" src="/assets/images/posts/20200419/matchfinding.apng" alt="Matchfinding animation" />
</div>